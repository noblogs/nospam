<?php
/**
 * @package NoSpam
 */
/*
Plugin Name: NoSpam
Plugin URI: http://spam.noblogs.org/
Description: IP-Agnostic anti-spam filter.
Version: 0.2.4
Author: Autistici/Inventati
Author UTI: http://www.inventati.org/
License: GPLv2
*/

define('NOSPAM_VERSION', '0.2.0');

if (!defined('NOSPAM_API_URL')) {
    define('NOSPAM_API_URL', 'https://nospam.autistici.org');
}

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
	exit;
}

function nospam_init() {
}
add_action('init', 'nospam_init');

function nospam_make_comment_for_training($comment, $train) {
    return array(
        'site' => get_option('home'),
        'name' => $comment->comment_author,
        'comment' => $comment->comment_content,
        'link' => $comment->comment_author_url,
        'email' => $comment->comment_author_email,
        'agent' => $comment->comment_agent,
        'version' => NOSPAM_VERSION,
        'train' => $train,
  );
}

function nospam_make_comment($commentdata) {
    return array(
        'site' => get_option('home'),
        'name' => $commentdata['comment_author'],
        'comment' => $commentdata['comment_content'],
        'link' => $commentdata['comment_author_url'],
        'email' => $commentdata['comment_author_email'],
        'agent' => $_SERVER['HTTP_USER_AGENT'],
        'version' => NOSPAM_VERSION,
    );
}

function nospam_json_query($method, $s) {
    $data_string = json_encode($s);
    $ch = curl_init(NOSPAM_API_URL . '/api/' . $method);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );
    $result = curl_exec($ch);
    curl_close($ch);
    return json_decode($result);
}

function nospam_auto_check_comment($commentdata) {
  $comment = nospam_make_comment($commentdata);
  $r = nospam_json_query('test', $comment);
  error_log('nospam_auto_check_comment(): ' . get_option('home') . ' -> ' . json_encode($r));
  if ($r && $r->category != 'ok') {
    add_filter('pre_comment_approved',
               function($a) { return 'spam'; }, 99);
  }
  return $commentdata;
}

add_action('preprocess_comment', 'nospam_auto_check_comment', 1);

function nospam_get_comment($comment_id) {
  global $wpdb;
  $comment_id = (int) $comment_id;
  $comment = $wpdb->get_row("SELECT * FROM $wpdb->comments WHERE comment_ID = '$comment_id'");
  if (!$comment) // it was deleted
    return 0;
  return $comment;
}

function nospam_submit_comment($comment, $category) {
  $cdata = nospam_make_comment_for_training($comment, $category);
  $r = nospam_json_query('classify', $cdata);
}

function nospam_submit_spam_comment($comment_id) {
    error_log('nospam_submit_spam_comment(): ' . get_option('home'));
    $comment = nospam_get_comment($comment_id);
    if (!$comment)
        return;
    if ($comment->comment_approved != 'spam')
        return;
    nospam_submit_comment($comment, 'spam');
}

function nospam_submit_nonspam_comment($comment_id) {
    error_log('nospam_submit_nonspam_comment(): ' . get_option('home'));
    $comment = nospam_get_comment($comment_id);
    if (!$comment)
        return;
    nospam_submit_comment($comment, 'ok');
}

function nospam_transition_comment_status($new_status, $old_status, $comment) {
  if ($new_status == $old_status)
    return;

  if ($new_status == 'spam') {
    nospam_submit_spam_comment($comment->comment_ID);
  } elseif ($old_status == 'spam' && ($new_status == 'approved' || $new_status == 'unapproved')) {
    nospam_submit_nonspam_comment($comment->comment_ID);
  }
}

add_action('transition_comment_status', 'nospam_transition_comment_status', 10, 3);
